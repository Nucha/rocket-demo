import { Component, Input, OnInit } from '@angular/core';
import { Launches } from '@interfaces/launches/launches';

@Component({
  selector: 'app-launch-item',
  templateUrl: './launch-item.component.html',
  styleUrls: ['./launch-item.component.scss'],
})
export class LaunchItemComponent implements OnInit {
  @Input() launch: any;
  @Input() index: any;

  title = 'Launch';

  constructor() {}

  ngOnInit(): void {}
}

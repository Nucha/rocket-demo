import { Component, OnDestroy, OnInit } from '@angular/core';

import { Subscription } from 'rxjs';

import { LaunchesService } from '@services/launches/launches.service';
import { Launches } from '@interfaces/launches/launches';

@Component({
  selector: 'app-launches',
  templateUrl: './launches.component.html',
  styleUrls: ['./launches.component.scss'],
})
export class LaunchesComponent implements OnInit, OnDestroy {
  launches: Launches[] = [];
  private subscriptions = new Subscription();

  constructor(private launchesService: LaunchesService) {}

  ngOnInit(): void {
    this.subscribeToLaunches();
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  private subscribeToLaunches(): any {
    const subs = this.launchesService
      .getLaunches()
      .subscribe(this.launchesHandler);
    this.subscriptions.add(subs);
  }

  private launchesHandler = (launchesList: Launches[]) => {
    this.launches = launchesList;
  };
}

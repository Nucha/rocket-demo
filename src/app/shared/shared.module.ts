import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';

const angularMaterial = [MatCardModule, MatButtonModule];

@NgModule({
  declarations: [],
  imports: [CommonModule, ...angularMaterial],
  exports: [CommonModule, ...angularMaterial],
})
export class SharedModule {}

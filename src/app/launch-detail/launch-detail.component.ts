import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Launches } from '@interfaces/launches/launches';
import { RocketsService } from '@services/rockets/rockets.service';

@Component({
  selector: 'app-launch-detail',
  templateUrl: './launch-detail.component.html',
  styleUrls: ['./launch-detail.component.scss'],
})
export class LaunchDetailComponent implements OnInit {
  rocket_id: any;
  flight_number: any;
  mission_name: any;
  rocketData: any;

  constructor(
    private route: ActivatedRoute,
    private rocketsService: RocketsService
  ) {}

  ngOnInit(): void {
    // this.route.queryParams.subscribe((params) => {
    //   this.rocket_id = params['rocket_id'];
    // });
    this.flight_number = 1;
    this.mission_name = 'FalconSat';
    this.rocket_id = 'falcon1';
    this.getRocketData(this.rocket_id);
  }

  getRocketData(rocket_id: any): any {
    this.rocketsService.getRocketById(rocket_id).subscribe((data) => {
      this.rocketData = data;
    });
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@environments/environment';

@Injectable({ providedIn: 'root' })
export class LaunchesService {
  constructor(private http: HttpClient) {}

  getLaunches(): any {
    return this.http.get<any>(environment.lunchesURL);
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@environments/environment';
import { filter } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class RocketsService {
  constructor(private http: HttpClient) {}

  getAllRockets(): any {
    return this.http.get<any>(environment.rocketUrl);
  }

  getRocketById(rocket_id: any) {
    return this.http.get<any>(environment.rocketUrl).pipe(
      filter((data: any) => {
        return data.rocket_id === rocket_id;
      })
    );
  }
}

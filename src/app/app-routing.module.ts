import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LaunchesComponent } from './launches/launches.component';
import { LaunchDetailComponent } from './launch-detail/launch-detail.component';

const routes: Routes = [
  { path: '', redirectTo: '/launches', pathMatch: 'full' },
  { path: 'launches', component: LaunchesComponent },
  { path: 'launchDetail/:rocket_id', component: LaunchDetailComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}

export interface Launches {
  flight_number: string;
  mission_name: string;
  rocket: {
    rocket_id: string;
    rocket_name: string;
  };
}
